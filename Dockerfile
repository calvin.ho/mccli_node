FROM node:14.11.0-buster

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install
RUN npm install pm2 -g

COPY . .
VOLUME /share/logs
EXPOSE 5000

CMD ["pm2-runtime", "start", "ecosystem.config.js"]
