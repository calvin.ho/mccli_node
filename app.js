const http = require('http');

const express = require('express');
const socketio = require('socket.io');
const ss = require('socket.io-stream');
const fs = require('fs');
const cors = require('cors');
const router = require('./router');
const jwt = require('jsonwebtoken');
const port = process.env.POST || 5000;
const app = express();
app.use(express.json());
app.use(cors());
const server = http.createServer(app);

const io = socketio(server); 
const User = require('./src/models/user.model');
const bcrypt =  require('bcrypt');

app.post('/createUser', (req, res) => { 
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.header('Access-Control-Allow-Headers', "*");
    res.setHeader('Access-Control-Allow-Credentials', true); // If needed
    const {username, password} = req.body;
    if(username && password) {
        const insertValues = {
            username: username,
            password: bcrypt.hashSync(password, 10) // 密碼加密
        };
        User.createUserAccount(insertValues, (data) => {
            console.log(data);
            if(!data.status) {
                res.send(500, data);
            }else {
                res.send(200, data);
            }         
        });
    }else {
        res.send(500, `nullpointexception`);
    }
});

app.post('/login', (req, res) => { 
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
    res.setHeader('Access-Control-Allow-Credentials', true); // If needed
    const {username, password} = req.body;
    if(username && password) {
        const insertValues = {
            username: username
        };
        User.selectUserInfo(insertValues, (data) => {
            if(!data.status) {
                res.send(500, data);
            }else {
                const [user] = JSON.parse(JSON.stringify(data.data));
                 bcrypt.compare(password, user.password, (err, result) => {
                    // 產生 JWT
                    if(result) {
                        const payload = {
                            username: user,
                        };
                        const token = jwt.sign({ payload, exp: Math.floor(Date.now() / 1000) + (60 * 15) }, 'key');
                        console.log(token);
                        res.send(200, {
                            'loginStatus': true,
                            'message': 'login success',
                            'token': token
                        })
                    }else {
                        res.send(200, {
                            'loginStatus': false,
                            'message': 'login failed',
                        })
                    }
                })
            }         
        });
    }else {
        res.send(500, `nullpointexception`);
    }
});


io.of('/logs').on('connection' ,async (socket) => {
    const callback = () => {   
        return new Promise(function (resolve, reject) {
            let stream = ss.createStream();  
            ss(socket).emit('file',stream);
             fs.createReadStream('/share/logs/latest.log').pipe(stream);
        });
    }

    callback().then(()=>{}).catch(err => console.log(err));

    fs.watchFile('/share/logs/latest.log', (curr, prev) => {
        const updateTrigger = (curr, prev) =>new Promise(function (resolve, reject) {
            if (curr.mtime - prev.mtime) {
                callback().then(()=>{
                    resolve(true)
                }).catch(err => {
                    console.log(err)
                    reject(true);
                });
            }
        });
        updateTrigger(curr, prev).then(()=>{}).catch(err => console.log(err));
    });
    
    socket.on('disconnect', function() {
        console.log('disconnect')
        socket.disconnect();
    })
});


app.use(router);
server.listen(port, ()=> console.log(`Server has started on post ${port}`));
