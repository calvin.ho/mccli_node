require('dotenv').config();
const mysql = require("mysql");

const connection = mysql.createPool({
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: 'mc288db',
  charset: 'utf8mb4'
});

module.exports = connection;